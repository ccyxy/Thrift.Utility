service  ProductService {
  string GetBrandByVendorSysNo(1:string request)
 
  string GetTop10NewsInfoList(1:string request)

  string EditSaleChannel(1:string request)
}

service  SOService {
  string GetSOSimpleInfo(1:string request)
}