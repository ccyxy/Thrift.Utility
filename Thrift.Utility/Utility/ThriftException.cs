﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thrift.Utility
{
    public class ThriftException : Exception
    {
        public enum ExceptionType
        {
            //超时异常
            Timeout,
            //服务端捕获异常
            ServerCaptured,
            //服务端未知异常
            ServerUnkown,
            //请求数据异常
            InvalidRequestPara
        }

        public ExceptionType type { get; set; }

        public ThriftException(ExceptionType type)
        {
            this.type = type;
        }

        public ThriftException(string message)
            : base(message)
        {
        }

        public ThriftException(ExceptionType type,string message)
            : base(message)
        {
            this.type = type;
        }
    }
}
