﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Thrift.Utility
{
    //标准返回，和OpenAPI的返回统一格式
    public class StandResponse<T>
    {
        /// <summary>
        /// 服务端成功调用：0
        /// 服务端业务异常：-1
        /// </summary>
        public string Code { get; set; }

        public string Desc { get; set; }

        public T Data { get; set; }
    }
}
